package com.example.preexamen

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class MainActivity : AppCompatActivity() {
    private lateinit var btnEntrar: Button
    private lateinit var btnSalir: Button
    private lateinit var txtUsuario: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnEntrar.setOnClickListener { entrar() }
        btnSalir.setOnClickListener { salir() }

    }

    private fun iniciarComponentes(){
        btnEntrar = findViewById(R.id.btnEntrar)
        btnSalir = findViewById(R.id.btnSalir)
        txtUsuario = findViewById(R.id.txtUsuario)

    }
    @SuppressLint("SuspiciousIndentation")
    private fun entrar(){
        val strUsuario: String = application.resources.getString(R.id.usuario)

        if ((txtUsuario.text.toString() == strUsuario)) {
            val bundle = Bundle()
            bundle.putString("usuario", strUsuario)

            val intent = Intent(this, CalculadoraActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
            return
        }
        Toast.makeText(this.applicationContext,"Usuario no valido", Toast.LENGTH_SHORT).show()
    }

    private fun salir(){
        val confirmar= AlertDialog.Builder(this)
        confirmar.setTitle("Calculo de pago de nomina")
        confirmar.setMessage("¿Quieres cerrar la aplicacion?")
        confirmar.setPositiveButton("Confirmar"){_,_->finish()}
        confirmar.setNegativeButton("Cancelar"){_,_->}.show()
    }
}